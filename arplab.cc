#include "node.h"
#include <arpa/inet.h>

#define ETH_HW_ADDR_LEN 6
#define IP_ADDR_LEN     4
#define LAST_OCTET      3
#define EMPTY           '\0'

#define IP_PROTO_TYPE   0x0800
#define ARP_FRAME_TYPE  0x0806
#define ETHER_HW_TYPE   0x0001
#define OP_ARP_REQUEST  0x0001
#define OP_ARP_REPLY    0x0002

/*
 * Implementar!
 * Intentar enviar `data` al `ip` especificado.
 * `data` es un buffer con IP_PAYLOAD_SIZE bytes.
 * Si la dirección MAC de ese IP es desconocida, debería enviarse un pedido ARP.
 * Devuelve 0 en caso de éxito y distinto de 0 si es necesario reintentar luego
 * (porque se está bucando la dirección MAC usando ARP)
 */
int Node::send_to_ip(IPAddress ip, void *data) {
    struct ether_frame eth_f;
    struct arp_pack arp;
    MACAddress my_mac;
    IPAddress my_ip;

    get_my_mac_address(my_mac);
    get_my_ip_address(my_ip);

    /* Si la dirección MAC de destino para esa ip es conocida, 
     * enviamos el paquete.
     */
    if(arp_table[ip[LAST_OCTET]][0] != EMPTY) {
        memcpy(eth_f.d_mac, arp_table[ip[LAST_OCTET]], ETH_HW_ADDR_LEN);
        memcpy(eth_f.s_mac, my_mac, ETH_HW_ADDR_LEN);
        eth_f.type = htons(IP_PROTO_TYPE);
        memcpy(eth_f.data, data, IP_PAYLOAD_SIZE);
        send_ethernet_packet(&eth_f);
        return 0;
    }

    /* La dirección MAC de destino no está en la tabla ARP, por lo tanto
     * construímos un ARP request para averiguarla.
     */
    memcpy(eth_f.s_mac, my_mac, ETH_HW_ADDR_LEN);
    memset(eth_f.d_mac, -1, ETH_HW_ADDR_LEN);
    eth_f.type = htons(ARP_FRAME_TYPE);
    arp.hard_type = htons(ETHER_HW_TYPE);
    arp.proto_type = htons(IP_PROTO_TYPE);
    arp.hard_size = ETH_HW_ADDR_LEN;
    arp.proto_size = IP_ADDR_LEN;
    arp.op = htons(OP_ARP_REQUEST);
    memcpy(arp.ip_source, my_ip, IP_ADDR_LEN);
    memcpy(arp.eth_source, my_mac, ETH_HW_ADDR_LEN);
    memset(arp.eth_dest, 0, ETH_HW_ADDR_LEN);
    memcpy(arp.ip_dest, ip, IP_ADDR_LEN);

    /* Encapsulamos el paquete ARP en la trama ethernet y hacemos broadcast. */
    memcpy(eth_f.data, (const void *)&arp, sizeof(arp));
    send_ethernet_packet(&eth_f);
    return 1;
}

/*
 * Implementar!
 * Manejar el recibo de un paquete.
 * Si es un paquete ARP: procesarlo.
 * Si es un paquete con datos: pasarlo a la capa de red con receive_ip_packet.
 * `packet` es un buffer de ETHERFRAME_SIZE bytes.
    Un paquete Ethernet tiene:
     - 6 bytes MAC destino
     - 6 bytes MAC origen
     - 2 bytes tipo
     - 46-1500 bytes de payload (en esta aplicación siempre son 1500)
    Tamaño total máximo: 1514 bytes
 */
void Node::receive_ethernet_packet(void *packet){
    struct ether_frame *eth_f = (struct ether_frame *)packet;
    struct arp_pack *arp;
    short merge = 0;
    MACAddress my_mac;
    IPAddress my_ip;

    get_my_mac_address(my_mac);
    get_my_ip_address(my_ip);

    /* Si la trama transporta un paquete IP hacia nosotros,
     * se lo pasamos a la capa de red.
     */
    if(ntohs(eth_f->type) == IP_PROTO_TYPE 
        && memcmp(eth_f->d_mac, my_mac, ETH_HW_ADDR_LEN) == 0){
        receive_ip_packet(&eth_f->data);
        return;
    }

    /* Si es un paquete ARP, lo atendemos según el algoritmo detallado en 
     * el RFC 826.
     */
    if(ntohs(eth_f->type) == ARP_FRAME_TYPE) {
        arp = (struct arp_pack *)eth_f->data;
        if(ntohs(arp->hard_type) != ETHER_HW_TYPE)
            return;
        if(ntohs(arp->proto_type) != IP_PROTO_TYPE)
            return;
        if(arp_table[arp->ip_source[LAST_OCTET]][0] != EMPTY) {
            memcpy(arp_table[arp->ip_source[LAST_OCTET]],
                arp->eth_source, ETH_HW_ADDR_LEN);
            merge = 1;
        }
        if(memcmp(arp->ip_dest, my_ip, IP_ADDR_LEN) != 0)
            return;

        if(!merge) {
            memcpy(arp_table[arp->ip_source[LAST_OCTET]],
                arp->eth_source, ETH_HW_ADDR_LEN);
        }

        if (ntohs(arp->op) != OP_ARP_REQUEST)
            return;

            /* Es un request ARP. Enviamos el reply con nuestra información. */
            memcpy(arp->eth_dest, arp->eth_source, ETH_HW_ADDR_LEN);
            memcpy(arp->eth_source, my_mac, ETH_HW_ADDR_LEN);
            memcpy(arp->ip_dest, arp->ip_source, IP_ADDR_LEN);
            memcpy(arp->ip_source, my_ip, IP_ADDR_LEN);

            memcpy(eth_f->d_mac, eth_f->s_mac, ETH_HW_ADDR_LEN);
            memcpy(eth_f->s_mac, my_mac, ETH_HW_ADDR_LEN);

            arp->op = htons(OP_ARP_REPLY);
            send_ethernet_packet(eth_f);
    }
}

/*
 * Constructor de la clase. Poner inicialización aquí.
 */
Node::Node()
{
    unsigned int i;
    timer = NULL;

    for (i = 0; i != AMOUNT_OF_CLIENTS; ++i) {
        seen[i] = 0;
    }

    /* Inicialización de la tabla ARP. */
    for (i = 0; i < 255; i++) {
        arp_table[i][0] = '\0';
    }
}
